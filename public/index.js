var mainContainer = $("#main_container");

var logout = function () {
  firebase
    .auth()
    .signOut()
    .then(
      function () {
        console.log("success");
        window.location.replace("login.html");
      },
      function () {}
    );
};

var init = function () {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // user details
        const userId = user.uid;
        const userName = user.displayName;
        var UID = userId;
        console.log('GISAPP.ConfigService.getApiKey', GISAPP.ConfigService.getApiKey)
        // CometChat.getUser(UID, GISAPP.ConfigService.getApiKey).then(
        //   user => {
        //         console.log("User details fetched for user:", user);
        //         // GISAPP.ChatService.cometChatLaunch();

                // login and launch chat here in embedded mode
                CometChatWidget.login({
                    uid: userId,
                }).then(
                (response) => {
                    CometChatWidget.launch({
                    widgetID: GISAPP.ConfigService.getWidgetID,
                    target: "#cometchat",
                    docked: 'true',
                    alignment: 'left', //left or right
                    roundedCorners: 'true',
                    height: "600px", //'450px',
                    width: "800px", //'400px',
                    defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
                    defaultType: 'user', //user or group
                    });
                },
                (error) => {
                    console.log("User login failed with error:", error);
                    var uid = userId;
                        var name = userName;
                        var user = new CometChat.User(uid);
                        user.setName(name);
                        CometChat.createUser(user, GISAPP.ConfigService.getAuthKey).then(
                            user => {
                                console.log("user created", user);
                                CometChatWidget.login({
                                    uid: userId,
                                }).then(
                                  (response) => {
                                      console.log("User login successful:", response);
                                      CometChatWidget.launch({
                                      widgetID: GISAPP.ConfigService.getWidgetID,
                                      target: "#cometchat",
                                      //docked: 'true',
                                      alignment: 'left', //left or right
                                      roundedCorners: 'true',
                                      height: '600px',
                                      width: '800px',
                                      defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
                                      defaultType: 'user', //user or group
                                      });
                                  },
                                  (error) => {
                                      console.error("User login failed with error:", error);
                                      //Check the reason for error and take appropriate action.
                                  }), 
                                error => {
                                  console.error("error", error);
                                }
                            }
                        )
                    //Check the reason for error and take appropriate action.
                }
            );
        // },
        // error => {
        //     console.log("User details fetching failed with error:", error);
        //     // create new user, login and launch chat here docked mode
        //     var uid = userId;
        //     var name = userName;
        //     var user = new CometChat.User(uid);
        //     user.setName(name);
        //     CometChat.createUser(user, GISAPP.ConfigService.getAuthKey).then(
        //         user => {
        //             console.log("user created", user);
        //             CometChatWidget.login({
        //                 uid: userId,
        //             }).then(
        //               (response) => {
        //                   console.log("User login successful:", response);
        //                   CometChatWidget.launch({
        //                   widgetID: GISAPP.ConfigService.getWidgetID,
        //                   target: "#cometchat",
        //                   docked: 'true',
        //                   alignment: 'left', //left or right
        //                   roundedCorners: 'true',
        //                   height: '450px',
        //                   width: '400px',
        //                   defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
        //                   defaultType: 'user', //user or group
        //                   });
        //               },
        //               (error) => {
        //                   console.error("User login failed with error:", error);
        //                   //Check the reason for error and take appropriate action.
        //               }), 
        //             error => {
        //               console.error("error", error);
        //             }
        //         }
        //     )
        // }
    // );
    mainContainer.css("display", "");
    } else {
      // No user is signed in logout user from the chat.
      CometChat.logout().then(
        () => {
          //Logout completed successfully
          console.log("Logout completed successfully");
        },
        (error) => {
          //Logout failed with exception
          console.log("Logout failed with exception:", { error });
        }
      );
      mainContainer.css("display", "none");
      window.location.replace("login.html");
    }
  });
};

init();