var fireBase = fireBase || firebase;
var hasInit = false;
var config = {
    // ENTER CONFIG HERE
    apiKey: "AIzaSyCveMl9DAlXlmjo0rGKeipsKz0F-WiZT7k",
    authDomain: "prime-cometchat.firebaseapp.com",
    projectId: "prime-cometchat",
    storageBucket: "prime-cometchat.appspot.com",
    messagingSenderId: "480991770380",
    appId: "1:480991770380:web:9d5522dcabe3f230633b29",
    measurementId: "G-E0GYN2G8MN"
  };

if(!hasInit){
    firebase.initializeApp(config);
    hasInit = true;
}

(function () {
  GISAPP.ChatService.cometChatInit();
  GISAPP.ChatService.cometChatWidgetInit();
})()