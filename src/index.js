var mainContainer = $('#main_container');

var logout = function () {
  firebase
    .auth()
    .signOut()
    .then(
      function () {
        console.log('success');
        window.location.replace('login.html');
      },
      function () {}
    );
};

var init = function () {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // user details
      const userId = user.uid;
      getUser('xxzw8j9us4uaagy5tueihxye7fh2');
      const userName = user.displayName;
      var UID = userId;
      // CometChat.getUser(UID, GISAPP.ConfigService.getAuthKey).then(
      //   (user) => {
      //     console.log('User details fetched for user:', user);
      // GISAPP.ChatService.cometChatLaunch();

      // login and launch chat here in embedded mode
      CometChatWidget.login({
        uid: userId,
      }).then(
        (response) => {
          CometChatWidget.launch({
            widgetID: GISAPP.ConfigService.getWidgetID,
            target: '#cometchat',
            docked: 'true',
            alignment: 'left', //left or right
            roundedCorners: 'true',
            height: '600px', //'450px',
            width: '800px', //'400px',
            defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
            defaultType: 'user', //user or group
          });
        },
        (error) => {
          console.log('User login failed with error:', error);
          var uid = userId;
          var name = userName;
          var user = new CometChat.User(uid);
          user.setName(name);
          CometChat.createUser(user, GISAPP.ConfigService.getAuthKey).then(
            (user) => {
              console.log('user created', user);
              CometChatWidget.login({
                uid: userId,
              }).then(
                (response) => {
                  console.log('User login successful:', response);
                  CometChat.getLoggedinUser().then(
                    (user) => {
                      console.log('user details:', { user });
                    },
                    (error) => {
                      console.log('error getting details:', { error });
                    }
                  );
                  CometChatWidget.launch({
                    widgetID: GISAPP.ConfigService.getWidgetID,
                    target: '#cometchat',
                    //docked: 'true',
                    alignment: 'left', //left or right
                    roundedCorners: 'true',
                    height: '600px',
                    width: '800px',
                    defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
                    defaultType: 'user', //user or group
                  });
                },
                (error) => {
                  console.error('User login failed with error:', error);
                  //Check the reason for error and take appropriate action.
                }
              ),
                (error) => {
                  console.error('error', error);
                };
            }
          );
          //Check the reason for error and take appropriate action.
        }
      );
      // },
      //   (error) => {
      //     console.log('User details fetching failed with error:', error);
      //     // create new user, login and launch chat here docked mode
      //     var uid = userId;
      //     var name = userName;
      //     var user = new CometChat.User(uid);
      //     user.setName(name);
      //     CometChat.createUser(user, GISAPP.ConfigService.getAuthKey).then(
      //       (user) => {
      //         console.log('user created', user);
      //         CometChatWidget.login({
      //           uid: userId,
      //         }).then(
      //           (response) => {
      //             console.log('User login successful:', response);
      //             CometChatWidget.launch({
      //               widgetID: GISAPP.ConfigService.getWidgetID,
      //               target: '#cometchat',
      //               docked: 'true',
      //               alignment: 'left', //left or right
      //               roundedCorners: 'true',
      //               height: '450px',
      //               width: '400px',
      //               defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
      //               defaultType: 'user', //user or group
      //             });
      //           },
      //           (error) => {
      //             console.error('User login failed with error:', error);
      //             //Check the reason for error and take appropriate action.
      //           }
      //         ),
      //           (error) => {
      //             console.error('error', error);
      //           };
      //       }
      //     );
      //   }
      // );
      mainContainer.css('display', '');
    } else {
      // No user is signed in logout user from the chat.
      CometChat.logout().then(
        () => {
          //Logout completed successfully
          console.log('Logout completed successfully');
        },
        (error) => {
          //Logout failed with exception
          console.log('Logout failed with exception:', { error });
        }
      );
      mainContainer.css('display', 'none');
      window.location.replace('login.html');
    }
  });
};
let listenerId = 'UNIQUE_LISTENER_ID';

CometChat.addMessageListener(
  'listenerId',
  new CometChat.MessageListener({
    onMessagesDelivered: (messageReceipt) => {
      console.log('MessageDeliverd', { messageReceipt });
    },
    onMessagesRead: (messageReceipt) => {
      console.log('MessageRead', { messageReceipt });
    },
  })
);
var limit = 30;

var groupsRequest = new CometChat.GroupsRequestBuilder()
  .setLimit(limit)
  .build();

groupsRequest.fetchNext().then(
  (groupList) => {
    /* groupList will be the list of Group class */
    console.log('Groups list fetched successfully', groupList);
    /* you can display the list of groups available using groupList */
  },
  (error) => {
    console.log('Groups list fetching failed with error', error);
  }
);

getUser = (uid) => {
  const promise = new Promise((resolve, reject) => {
    if (!uid) {
      const error = { code: 'UID_NOT_AVAILABLE' };
      reject(error);
    }
    // jsonp('https://api-eu.cometchat.io/v2.0/users/xxzw8j9us4uaagy5tueihxye7fh2')
    //   .then((user) => resolve(user))
    //   .catch((error) => reject(error));

    CometChat.getUser(uid)
      // fetch('https://api-eu.cometchat.io/v2.0/users/xxzw8j9us4uaagy5tueihxye7fh2')
      .then((user) => resolve(user))
      .catch((error) => reject(error));
  });

  return promise;
};
init();

let jsonpID = 0;

function jsonp(url, timeout = 7500) {
  const head = document.querySelector('head');
  jsonpID += 1;

  return new Promise((resolve, reject) => {
    let script = document.createElement('script');
    const callbackName = `jsonpCallback${jsonpID}`;

    script.src = encodeURI(
      `${url}?appId=33859c68b3afda4&apiKey=18732ea42fa0715399da0a50cabc1546bd2c57b2&callback=${callbackName}`
    );
    script.async = true;

    const timeoutId = window.setTimeout(() => {
      cleanUp();

      return reject(new Error('Timeout'));
    }, timeout);

    window[callbackName] = (data) => {
      cleanUp();

      return resolve(data);
    };

    script.addEventListener('error', (error) => {
      cleanUp();

      return reject(error);
    });

    function cleanUp() {
      window[callbackName] = undefined;
      head.removeChild(script);
      window.clearTimeout(timeoutId);
      script = null;
    }

    head.appendChild(script);
  });
}
jsonp(
  'https://api-eu.cometchat.io/v2.0/users/xxzw8j9us4uaagy5tueihxye7fh2',
  function (json) {
    console.log(json);
  }
);
