GISAPP.LoginService = (function () {
    var _scope = {};
    _scope.createUser = function (name, uid) {
        console.log('createuser', name, uid);
        var user = new CometChat.User(uid);
        user.setName(name);
        CometChat.createUser(user, GISAPP.ConfigService.getAuthKey).then(
            (user) => {
                console.log('user created', user);
            },
            (error) => {
                console.log('error', error);
            }
        );
    }
    _scope.getLoggedUser = function(){
        CometChat.getLoggedinUser().then(
            user => {
              console.log("user details:", { user });
            },
            error => {
              console.log("error getting details:", { error });
            }
        );
    }
        
    return {
        createUser: _scope.createUser,
        getLoggedUser: _scope.getLoggedUser
    }
})();
