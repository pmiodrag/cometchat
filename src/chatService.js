GISAPP.ChatService = (function () {
    var _scope = {};
    _scope.LOGTAG = 'ChatService';
    var appID = GISAPP.ConfigService.getAppID;
        var appRegion =  GISAPP.ConfigService.getAppRegion;
        var authKey = GISAPP.ConfigService.getAuthKey;
        var appSetting = new CometChat.AppSettingsBuilder()
          .subscribePresenceForAllUsers()
          .setRegion(appRegion)
          .build();
          console.log('appSetting', appSetting);

    _scope.cometChatInit = function() {
      console.log("Initialization completed successfully");

      CometChat.init(appID, appSetting).then(
          () => {
            console.log("Initialization completed successfully", appSetting);
            // You can now call login function.
          },
          (error) => {
            console.log("Initialization failed with error:", error, appSetting);
            // Check the reason for error and take appropriate action.
          });
      }

  _scope.cometChatWidgetInit = function() {
    window.addEventListener("DOMContentLoaded", (event) => {
      CometChatWidget.init({
          appID: appID,
          appRegion:  appRegion,
          authKey: authKey,
      }).then(
        (response) => {
          console.log("Initialization (CometChatWidget) completed successfully");
        },
        (error) => {
          console.log("Initialization (CometChatWidget) failed with error:", error);
          //Check the reason for error and take appropriate action.
        }
      );
    });
  };
  _scope.cometChatLaunch = function() {
    CometChatWidget.login({
      uid: userId,
  }).then(
    (response) => {
        console.log("User login successful:", response);
        CometChatWidget.launch({
          widgetID: GISAPP.ConfigService.getWidgetID,
          target: "#cometchat",
          docked: 'true',
          alignment: 'left', //left or right
          roundedCorners: 'true',
          height: '450px',
          width: '400px',
          defaultID: 'superhero1', //default UID (user) or GUID (group) to show,
          defaultType: 'user', //user or group
          });
    },
    (error) => {
        console.error("User login failed with error:", error);
        //Check the reason for error and take appropriate action.
    }), 
    error => {
      console.error("error", error);
    }
  };

  return {
    cometChatInit: _scope.cometChatInit,
    cometChatWidgetInit: _scope.cometChatWidgetInit,
    cometChatLaunch: _scope.cometChatLaunch,
  };
})();