GISAPP.ConfigService = (function(){
    // We need to wrap this inside a function to have an accessible private scope so we don't inject directly in the global scope
    var _scope = {};

    // INJECTION
    // server variables
    _scope.authKey = '591f1a58de9313b281aacbd4e3c0a6cd51c17c6a';
    _scope.appID = '33859c68b3afda4';
    _scope.appRegion = 'eu';
    _scope.widgetID = '9843bf4c-2c6e-4c12-b340-90356f47b36e';
    _scope.apiKey = '18732ea42fa0715399da0a50cabc1546bd2c57b2';

    return {
        getAuthKey: function() {
            return _scope.authKey;
        }(),
        getAppID: function() {
            return _scope.appID;
        }(),
        getAppRegion: function() {
            return _scope.appRegion;
        }(),
        getWidgetID: function() {
            return _scope.widgetID;
        }(),
        getApiKey: function() {
            return _scope.apiKey;
        }(),
    }
})();